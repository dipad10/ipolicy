﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IPolicy.ViewModels.SearchFilters;

using System.Web.Mvc;
using Ipolicy.Data.Repository;
using IPolicy.Data.Mod_Drive;
using IPolicy.Data.Repository;
using IPolicy.ViewModels.Admin;

namespace IPolicy.Controllers
{
    [Filters.Authorizesession]

    public class AdminController : Controller
    {

        // GET: Admin
        public ActionResult InsuredClients()
        {
            var insuredclients = new CLS_ABSINSRDTAB().Getall();
            return View(insuredclients);
        }
        //GET
        public ActionResult InsuredClients_edit(int? id)
        {
            if (id != null)
            {
                //Edit mode
                var insured = new CLS_ABSINSRDTAB().GetinsuredVMByID(id);
                ViewBag.insuredtypes = new Filldropdownhelper().FillInsuredtypes();
                ViewBag.states = new Filldropdownhelper().Fillstates();
                ViewBag.lga = new Filldropdownhelper().Fillstates();
                ViewBag.pagetitle = "Edit Insured - " + id + "";
                return View(insured);
            }
            else
            {
                //New Instance
                InsuredClients_editVM insured = new InsuredClients_editVM();
                ViewBag.insuredtypes = new Filldropdownhelper().FillInsuredtypes();
                ViewBag.states = new Filldropdownhelper().Fillstates();
                ViewBag.lga = new Filldropdownhelper().Fillstates();
                ViewBag.pagetitle = "New Insured";

                return View(insured);
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult InsuredClients_edit(InsuredClients_editVM vm)
        {
            if(ModelState.IsValid)
            {

            }

            return View(vm);
        }


        public ActionResult Users()
        {
            ViewBag.pagetitle = "Security Users";
            ViewBag.users = new CLS_ABSPASSTAB().Getuserslist();
            return View();
        }
        [HttpPost]
        public ActionResult Users(UsersSearchModel searchModel)
        {
            ViewBag.pagetitle = "Security Users";
            //var search = new UsersSearchModel();
            ViewBag.users = new CLS_ABSPASSTAB().GetUserssearch(searchModel);
            return View();
        }

        public ActionResult Users_edit(int? id)
        {
            if (id != null)
            {
                //Edit mode
                ViewBag.userdepartments = new Filldropdownhelper().Filluserdepartments();
                ViewBag.usertypes = new Filldropdownhelper().Fillusertypes();
                ViewBag.usergroup = new Filldropdownhelper().FilluserGroups();
                ViewBag.pagetitle = "Edit User - " + id + "";

                var user = new CLS_ABSPASSTAB().GetuserVMByID(id);
                return View(user);
            }
            else
            {
                //New Instance
                Users_editVM user = new Users_editVM();
                ViewBag.userdepartments = new Filldropdownhelper().Filluserdepartments();
                ViewBag.usertypes = new Filldropdownhelper().Fillusertypes();
                ViewBag.usergroup = new Filldropdownhelper().FilluserGroups();
                ViewBag.pagetitle = "New User";

                return View(user);
            }


        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Users_edit(Users_editVM vm)
        {
            //check model validations
            if(ModelState.IsValid)
            {
                ModDrive.Response res = new CLS_ABSPASSTAB().InsertorUpdateUser(vm);
                if(res.ErrorCode == 0)
                {
                    //successfull
                    TempData["success"] = true;
                    TempData["message"] = "Data saved successfully.";
                    return RedirectToAction("Users");

                }
                else
                {
                    //display error panel
                    ViewBag.message = res.ErrorMessage + " " + res.ExtraMessage;
                    ViewBag.messagetype = "error";
                    ViewBag.pagetitle = "Check Postings Carefully";

                    ViewBag.userdepartments = new Filldropdownhelper().Filluserdepartments();
                    ViewBag.usertypes = new Filldropdownhelper().Fillusertypes();
                    ViewBag.usergroup = new Filldropdownhelper().FilluserGroups();
                    return View(vm);


                }
            }
            //fill back the dropdowns
            ViewBag.pagetitle = "Check Postings Carefully";

            ViewBag.userdepartments = new Filldropdownhelper().Filluserdepartments();
            ViewBag.usertypes = new Filldropdownhelper().Fillusertypes();
            ViewBag.usergroup = new Filldropdownhelper().FilluserGroups();

            return View(vm);
        }

    }
}