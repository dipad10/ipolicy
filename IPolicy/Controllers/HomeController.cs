﻿using Ipolicy.Data.Repository;
using IPolicy.ViewModels.Home;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IPolicy.Controllers
{
    public class HomeController : Controller
    {

        public ActionResult Login(string returnurl, string msg)
        {
            if(msg != null)
            {
                ViewBag.message = "You have successfully Signed out. Thank you";
                ViewBag.messagetype = "success";
            }
            LoginVM vm = new LoginVM();
            ViewBag.Title = "Ipolicy Login";
            return View(vm);
        }


        [HttpPost]
        public ActionResult Login(LoginVM vm, string returnurl)
        {
            if (ModelState.IsValid)
            {
               // aunthenticate login
                var login = new CLS_ABSPASSTAB().Authenticatelogin(vm);
                if (login != null)
                {
                    Session["currentuser"] = vm.Username;
                    TempData["success"] = true;
                    TempData["message"] = "Login successful! Welcome back " + vm.Username + " ";
                    if(returnurl !=null)
                    {
                        return Redirect(returnurl);
                    }
                    else { return Redirect("/Underwriting/Dashboard"); }
                    
                   
                }
                else
                {

                    ViewBag.message = "Please enter a correct username and password. Note that both fields may be case-sensitive";
                    ViewBag.messagetype = "error";
                    return View(vm);
                }

            }
            return View(vm);

        }

    

        public ActionResult Logout()
        {
            Session.Clear();
            return RedirectToAction("Login");
        }

    }
}