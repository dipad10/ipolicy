﻿using IPolicy.Data.Entities;
using IPolicy.Data.Mod_Drive;
using IPolicy.ViewModels.Admin;
using IPolicy.ViewModels.Home;
using IPolicy.ViewModels.SearchFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ipolicy.Data.Repository
{
    public class CLS_ABSPASSTAB
    {
        Ipolicy_DBEntities db = new Ipolicy_DBEntities();
        ModDrive mod = new ModDrive();

        public IQueryable<ABSPASSTAB> GetUserssearch(UsersSearchModel searchModel)
        {
            //if there is filter select all aby filter
            var result = db.ABSPASSTABs.AsQueryable();

            if (searchModel != null)
            {


                if (!string.IsNullOrEmpty(searchModel.Username))
                    result = result.Where(x => x.PWD_USER_NAME.Contains(searchModel.Username));
                if (searchModel.From.HasValue)
                    result = result.Where(x => x.PWD_KEYDTE >= searchModel.From);
                if (searchModel.To.HasValue)
                    result = result.Where(x => x.PWD_KEYDTE <= searchModel.To);

            }

            return result.OrderByDescending(p => p.PWD_REC_NO);

            //return db.ABSPASSTABs.OrderByDescending(p => p.PWD_REC_NO).ToList().Take(50);
        }
        public IEnumerable<ABSPASSTAB> Getuserslist()
        {
            //select just 50 records to avoid hanging
            return db.ABSPASSTABs.OrderByDescending(p => p.PWD_REC_NO).ToList().Take(50);

        }

        public ABSPASSTAB GetuserbyID(int? id)
        {
            return db.ABSPASSTABs.Where(p => p.PWD_REC_NO == id).FirstOrDefault();
        }
        public Users_editVM GetuserVMByID(int? id)
        {
            Users_editVM vm = new Users_editVM();
            var user = db.ABSPASSTABs.Where(p => p.PWD_REC_NO == id).FirstOrDefault();
            vm.PWD_ACCOUNTS_SW = user.PWD_ACCOUNTS_SW;
            vm.PWD_ACCOUNTS_SW_GL = user.PWD_ACCOUNTS_SW_GL;
            vm.PWD_CLAIMS_SW = user.PWD_CLAIMS_SW;
            vm.PWD_CODE = mod.Decrypt(user.PWD_CODE);
            vm.PWD_CODE_RE = mod.Decrypt(user.PWD_CODE_RE);
            vm.PWD_CREDITCONTROL_SW = user.PWD_CREDITCONTROL_SW;
            vm.PWD_DELETE_SW = user.PWD_DELETE_SW;
            vm.PWD_EMAIL_NUM = user.PWD_EMAIL_NUM;
            vm.PWD_ENABLE_SW = user.PWD_ENABLE_SW;
            vm.PWD_FAX_NUM = user.PWD_FAX_NUM;
            vm.PWD_FIXEDASSET_SW = user.PWD_FIXEDASSET_SW;
            vm.PWD_GENERAL_SW = user.PWD_GENERAL_SW;
            vm.PWD_ID = user.PWD_ID;
            vm.PWD_INSERT_SW = user.PWD_INSERT_SW;
            vm.PWD_INVESTMENT_SW = user.PWD_INVESTMENT_SW;
            vm.PWD_MARKETING_SW = user.PWD_MARKETING_SW;
            vm.PWD_MGT_CAT_NUM = user.PWD_MGT_CAT_NUM;
            //vm.PWD_MGT_CAT_SW == user.PWD_MGT_CAT_SW;
            vm.PWD_MOBILE_NUM = user.PWD_MOBILE_NUM;
            vm.PWD_PAYROLL_SW = user.PWD_PAYROLL_SW;
            vm.PWD_PERSONNEL_SW = user.PWD_PERSONNEL_SW;
            vm.PWD_REC_NO = Convert.ToInt32(user.PWD_REC_NO);
            vm.PWD_POLICY_SW = user.PWD_POLICY_SW;
            vm.PWD_PRINT_SW = user.PWD_PRINT_SW;
            vm.PWD_PROCESS_SW = user.PWD_PROCESS_SW;
            vm.PWD_PURCHASES_SW = user.PWD_PURCHASES_SW;
            vm.PWD_REINSURANCE_SW = user.PWD_REINSURANCE_SW;
            vm.PWD_SECURITY_SW = user.PWD_SECURITY_SW;
            vm.PWD_SETUP_SW = user.PWD_SETUP_SW;
            vm.PWD_TEL_NUM = user.PWD_TEL_NUM;
            vm.PWD_UNDERWRITING_SW = user.PWD_UNDERWRITING_SW;
            vm.PWD_UND_SPC = user.PWD_UND_SPC;
            vm.PWD_UPDATE_SW = user.PWD_UPDATE_SW;
            vm.PWD_USER_DEPT = user.PWD_USER_DEPT;
            vm.PWD_USER_GRP = user.PWD_USER_GRP;
            vm.PWD_USER_NAME = user.PWD_USER_NAME;
            vm.PWD_USER_TYPE = user.PWD_USER_TYPE;
            vm.PWD_VIEW_SW = user.PWD_VIEW_SW;
            vm.PWD_EXPIRY_DATE = user.PWD_EXPIRY_DATE;
            return vm;
        }


        public ABSPASSTAB Getuserbyemail(string email)
        {
            return db.ABSPASSTABs.Where(p => p.PWD_EMAIL_NUM == email).FirstOrDefault();
        }

        public ABSPASSTAB Authenticatelogin(LoginVM login)
        {
            var hashpwd = new ModDrive().Encrypt(login.Password);
            
            return db.ABSPASSTABs.Where(p => p.PWD_USER_NAME == login.Username && p.PWD_CODE == hashpwd).FirstOrDefault();
        }

        public ModDrive.Response InsertorUpdateUser(Users_editVM vm)
        {
            if (vm.PWD_REC_NO > 0)
            {
                try
                {
                    //do update
                    ABSPASSTAB user = GetuserbyID(vm.PWD_REC_NO);
                    user.PWD_ACCOUNTS_SW = vm.PWD_ACCOUNTS_SW;
                    user.PWD_ACCOUNTS_SW_GL = vm.PWD_ACCOUNTS_SW_GL;
                    user.PWD_CLAIMS_SW = vm.PWD_CLAIMS_SW;
                    user.PWD_CODE = mod.Encrypt(vm.PWD_CODE);
                    user.PWD_CODE_RE = mod.Encrypt(vm.PWD_CODE_RE);
                    user.PWD_CREDITCONTROL_SW = vm.PWD_CREDITCONTROL_SW;
                    user.PWD_DELETE_SW = vm.PWD_DELETE_SW;
                    user.PWD_EMAIL_NUM = vm.PWD_EMAIL_NUM;
                    user.PWD_ENABLE_SW = vm.PWD_ENABLE_SW;
                    user.PWD_FAX_NUM = vm.PWD_FAX_NUM;
                    user.PWD_FIXEDASSET_SW = vm.PWD_FIXEDASSET_SW;
                    user.PWD_GENERAL_SW = vm.PWD_GENERAL_SW;
                    user.PWD_INSERT_SW = vm.PWD_INSERT_SW;
                    user.PWD_INVESTMENT_SW = vm.PWD_INVESTMENT_SW;
                    user.PWD_MARKETING_SW = vm.PWD_MARKETING_SW;
                    user.PWD_MGT_CAT_NUM = vm.PWD_MGT_CAT_NUM;
                    //vm.PWD_MGT_CAT_SW == user.PWD_MGT_CAT_SW;
                    user.PWD_MOBILE_NUM = vm.PWD_MOBILE_NUM;
                    user.PWD_PAYROLL_SW = vm.PWD_PAYROLL_SW;
                    user.PWD_PERSONNEL_SW = vm.PWD_PERSONNEL_SW;
                    user.PWD_POLICY_SW = vm.PWD_POLICY_SW;
                    user.PWD_PRINT_SW = vm.PWD_PRINT_SW;
                    user.PWD_PROCESS_SW = vm.PWD_PROCESS_SW;
                    user.PWD_PURCHASES_SW = vm.PWD_PURCHASES_SW;
                    user.PWD_REINSURANCE_SW = vm.PWD_REINSURANCE_SW;
                    user.PWD_SECURITY_SW = vm.PWD_SECURITY_SW;
                    user.PWD_SETUP_SW = vm.PWD_SETUP_SW;
                    user.PWD_TEL_NUM = vm.PWD_TEL_NUM;
                    user.PWD_UNDERWRITING_SW = vm.PWD_UNDERWRITING_SW;
                    user.PWD_UND_SPC = vm.PWD_UND_SPC;
                    user.PWD_UPDATE_SW = vm.PWD_UPDATE_SW;
                    user.PWD_USER_DEPT = vm.PWD_USER_DEPT;
                    user.PWD_USER_GRP = vm.PWD_USER_GRP;
                    user.PWD_USER_NAME = vm.PWD_USER_NAME.ToUpper();
                    user.PWD_USER_TYPE = vm.PWD_USER_TYPE;
                    user.PWD_VIEW_SW = vm.PWD_VIEW_SW;
                    user.PWD_ALERT_DATE = DateTime.Now;
                    user.PWD_EFFECTIVE_DATE = DateTime.Now;
                    user.PWD_EXPIRY_DATE = DateTime.Now.AddDays(30);
                    user.PWD_FLAG = "C";
                    user.PWD_OPERID = mod.Getcurrentuser();

                    db.SaveChanges();
                    return ModDrive._GetResponseStruct(ModDrive.ErrCodeEnum.NO_ERROR, 1, 0);
                }
                catch (Exception ex)
                {

                    return ModDrive._GetResponseStruct(ModDrive.ErrCodeEnum.GENERIC_ERROR, 0, 1, ex.Message);
                }

            }
            else
            {
                try
                {
                    //insert new insured
                    ABSPASSTAB user = new ABSPASSTAB();
                    user.PWD_ACCOUNTS_SW = vm.PWD_ACCOUNTS_SW;
                    user.PWD_ACCOUNTS_SW_GL = vm.PWD_ACCOUNTS_SW_GL;
                    user.PWD_CLAIMS_SW = vm.PWD_CLAIMS_SW;
                    user.PWD_CODE = mod.Encrypt(vm.PWD_CODE);
                    user.PWD_CODE_RE = mod.Encrypt(vm.PWD_CODE_RE);
                    user.PWD_CREDITCONTROL_SW = vm.PWD_CREDITCONTROL_SW;
                    user.PWD_DELETE_SW = vm.PWD_DELETE_SW;
                    user.PWD_EMAIL_NUM = vm.PWD_EMAIL_NUM;
                    user.PWD_ENABLE_SW = vm.PWD_ENABLE_SW;
                    user.PWD_FAX_NUM = vm.PWD_FAX_NUM;
                    user.PWD_FIXEDASSET_SW = vm.PWD_FIXEDASSET_SW;
                    user.PWD_GENERAL_SW = vm.PWD_GENERAL_SW;
                    user.PWD_ID = mod.GetpassCNT();
                    user.PWD_GROUP_ID = "001";
                    user.PWD_INSERT_SW = vm.PWD_INSERT_SW;
                    user.PWD_INVESTMENT_SW = vm.PWD_INVESTMENT_SW;
                    user.PWD_MARKETING_SW = vm.PWD_MARKETING_SW;
                    user.PWD_MGT_CAT_NUM = vm.PWD_MGT_CAT_NUM;
                    //vm.PWD_MGT_CAT_SW == user.PWD_MGT_CAT_SW;
                    user.PWD_MOBILE_NUM = vm.PWD_MOBILE_NUM;
                    user.PWD_PAYROLL_SW = vm.PWD_PAYROLL_SW;
                    user.PWD_PERSONNEL_SW = vm.PWD_PERSONNEL_SW;
                    user.PWD_POLICY_SW = vm.PWD_POLICY_SW;
                    user.PWD_PRINT_SW = vm.PWD_PRINT_SW;
                    user.PWD_PROCESS_SW = vm.PWD_PROCESS_SW;
                    user.PWD_PURCHASES_SW = vm.PWD_PURCHASES_SW;
                    user.PWD_REINSURANCE_SW = vm.PWD_REINSURANCE_SW;
                    user.PWD_SECURITY_SW = vm.PWD_SECURITY_SW;
                    user.PWD_SETUP_SW = vm.PWD_SETUP_SW;
                    user.PWD_TEL_NUM = vm.PWD_TEL_NUM;
                    user.PWD_UNDERWRITING_SW = vm.PWD_UNDERWRITING_SW;
                    user.PWD_UND_SPC = vm.PWD_UND_SPC;
                    user.PWD_UPDATE_SW = vm.PWD_UPDATE_SW;
                    user.PWD_USER_DEPT = vm.PWD_USER_DEPT;
                    user.PWD_USER_GRP = vm.PWD_USER_GRP;
                    user.PWD_USER_NAME = vm.PWD_USER_NAME.ToUpper();
                    user.PWD_USER_TYPE = vm.PWD_USER_TYPE;
                    user.PWD_VIEW_SW = vm.PWD_VIEW_SW;
                    user.PWD_ALERT_DATE = DateTime.Now;
                    user.PWD_EFFECTIVE_DATE = DateTime.Now;
                    user.PWD_EXPIRY_DATE = DateTime.Now.AddDays(30);
                    user.PWD_FLAG = "A";
                    user.PWD_KEYDTE = DateTime.Now;
                    user.PWD_OPERID = mod.Getcurrentuser();
                    //update autonumber
                    db.ABSPASSTABs.Add(user);
                    db.SaveChanges();
                    mod.UpdatepassCNT();

                    return ModDrive._GetResponseStruct(ModDrive.ErrCodeEnum.NO_ERROR, 1, 0);
                }
                catch (Exception ex)
                {

                    return ModDrive._GetResponseStruct(ModDrive.ErrCodeEnum.GENERIC_ERROR, 0, 1, ex.Message);
                }

            }


        }


    }
}
