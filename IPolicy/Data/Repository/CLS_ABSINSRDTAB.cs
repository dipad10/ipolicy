﻿using IPolicy.Data.Entities;
using IPolicy.Data.Mod_Drive;
using IPolicy.ViewModels.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IPolicy.Data.Repository
{
    public class CLS_ABSINSRDTAB
    {
        Ipolicy_DBEntities db = new Ipolicy_DBEntities();
        ModDrive mod = new ModDrive();

        public IEnumerable<ABSINSRDTAB> Getall()
        {
           
            return db.ABSINSRDTABs.OrderByDescending(p => p.CTINSRD_NO).ToList().Take(50);
        }

        public ABSINSRDTAB GetinsuredByID(int? id)
        {
            return db.ABSINSRDTABs.Where(p => p.CTINSRD_NO == id).FirstOrDefault();
        }

        public InsuredClients_editVM GetinsuredVMByID(int? id)
        {
            InsuredClients_editVM vm = new InsuredClients_editVM();
            var insured = db.ABSINSRDTABs.Where(p => p.CTINSRD_NO == id).FirstOrDefault();
            vm.CtinsrdAddr1 = insured.CTINSRD_ADDR1;
            vm.CtinsrdAddr2 = insured.CTINSRD_ADDR2;
            vm.CtinsrdAddr3 = insured.CTINSRD_ADDR3;
            vm.CtinsrdBizorigin = insured.CTINSRD_BIZORIGIN;
            vm.CtinsrdBvNum = insured.CTINSRD_BV_NUM;
            vm.CtinsrdDob = insured.CTINSRD_DOB;
            vm.CtinsrdEmailNum = insured.CTINSRD_EMAIL_NUM;
            vm.CtinsrdGender = insured.CTINSRD_GENDER;
            vm.CtinsrdGsmNum = insured.CTINSRD_GSM_NUM;
            vm.CtinsrdLga = insured.CTINSRD_LGA;
            vm.CtinsrdLongDescr = insured.CTINSRD_LONG_DESCR;
            vm.CtinsrdNation = insured.CTINSRD_NATION;
            vm.CtinsrdNo = Convert.ToInt32(insured.CTINSRD_NO);
            vm.CtinsrdOccup = insured.CTINSRD_OCCUP;
            vm.CtinsrdOrigin = insured.CTINSRD_ORIGIN;
            vm.CtinsrdOthername = insured.CTINSRD_OTHERNAME;
            vm.CtinsrdShortDescr = insured.CTINSRD_SHORT_DESCR;
            vm.CtinsrdTelNum1 = insured.CTINSRD_TEL_NUM1;
            vm.CtinsrdTelNum2 = insured.CTINSRD_TEL_NUM2;
            vm.CtinsrdType = insured.CTINSRD_TYPE;

            return vm;
        }
        public ModDrive.Response InsertorUpdateInsured(InsuredClients_editVM vm)
        {
            if (vm.CtinsrdNo > 0)
            {
                try
                {
                    //do update
                    ABSINSRDTAB insured = GetinsuredByID(vm.CtinsrdNo);
                    insured.CTINSRD_ADDR1 = vm.CtinsrdAddr1;
                    insured.CTINSRD_ADDR2 = vm.CtinsrdAddr2;
                    insured.CTINSRD_ADDR3 = vm.CtinsrdAddr3;
                    insured.CTINSRD_BIZORIGIN = vm.CtinsrdBizorigin;
                    insured.CTINSRD_BV_NUM = vm.CtinsrdBvNum;
                    insured.CTINSRD_DOB = vm.CtinsrdDob;
                    insured.CTINSRD_EMAIL_NUM = vm.CtinsrdEmailNum;
                    insured.CTINSRD_GENDER = vm.CtinsrdGender;
                    insured.CTINSRD_GSM_NUM = vm.CtinsrdGsmNum;
                    insured.CTINSRD_LGA = vm.CtinsrdLga;
                    insured.CTINSRD_LONG_DESCR = vm.CtinsrdLongDescr;
                    insured.CTINSRD_NATION = vm.CtinsrdNation;
                    insured.CTINSRD_OCCUP = vm.CtinsrdOccup;
                    insured.CTINSRD_OPERID = Guid.NewGuid().ToString();
                    insured.CTINSRD_ORIGIN = vm.CtinsrdOrigin;
                    insured.CTINSRD_OTHERNAME = vm.CtinsrdOthername;
                    insured.CTINSRD_SHORT_DESCR = vm.CtinsrdShortDescr;
                    insured.CTINSRD_TEL_NUM1 = vm.CtinsrdTelNum1;
                    insured.CTINSRD_TEL_NUM2 = vm.CtinsrdTelNum2;
                    insured.CTINSRD_TYPE = vm.CtinsrdType;
                    insured.CTINSRD_FLAG = "C";
                    db.SaveChanges();
                    return ModDrive._GetResponseStruct(ModDrive.ErrCodeEnum.NO_ERROR, 1, 0);
                }
                catch (Exception ex)
                {

                    return ModDrive._GetResponseStruct(ModDrive.ErrCodeEnum.GENERIC_ERROR, 0, 1, ex.Message);
                }

            }
            else
            {
                try
                {
                    //insert new insured
                    ABSINSRDTAB insured = new ABSINSRDTAB();

                    insured.CTINSRD_ID = "001";
                    insured.CTINSRD_NUM = mod.GetinsuredCNT();
                    insured.CTINSRD_ADDR1 = vm.CtinsrdAddr1;
                    insured.CTINSRD_ADDR2 = vm.CtinsrdAddr2;
                    insured.CTINSRD_ADDR3 = vm.CtinsrdAddr3;
                    insured.CTINSRD_BIZORIGIN = vm.CtinsrdBizorigin;
                    insured.CTINSRD_BV_NUM = vm.CtinsrdBvNum;
                    insured.CTINSRD_DOB = vm.CtinsrdDob;
                    insured.CTINSRD_EMAIL_NUM = vm.CtinsrdEmailNum;
                    insured.CTINSRD_GENDER = vm.CtinsrdGender;
                    insured.CTINSRD_GSM_NUM = vm.CtinsrdGsmNum;
                    insured.CTINSRD_LGA = vm.CtinsrdLga;
                    insured.CTINSRD_LONG_DESCR = vm.CtinsrdLongDescr;
                    insured.CTINSRD_NATION = vm.CtinsrdNation;
                    insured.CTINSRD_OCCUP = vm.CtinsrdOccup;
                    insured.CTINSRD_OPERID = Guid.NewGuid().ToString();
                    insured.CTINSRD_ORIGIN = vm.CtinsrdOrigin;
                    insured.CTINSRD_OTHERNAME = vm.CtinsrdOthername;
                    insured.CTINSRD_SHORT_DESCR = vm.CtinsrdShortDescr;
                    insured.CTINSRD_TEL_NUM1 = vm.CtinsrdTelNum1;
                    insured.CTINSRD_TEL_NUM2 = vm.CtinsrdTelNum2;
                    insured.CTINSRD_TYPE = vm.CtinsrdType;
                    insured.CTINSRD_KEYDTE = DateTime.Now;
                    insured.CTINSRD_FLAG = "A";
                    //update autonumber
                    mod.UpdateinsuredCNT();
                    db.ABSINSRDTABs.Add(insured);
                    return ModDrive._GetResponseStruct(ModDrive.ErrCodeEnum.NO_ERROR, 1, 0);
                }
                catch (Exception ex)
                {

                    return ModDrive._GetResponseStruct(ModDrive.ErrCodeEnum.GENERIC_ERROR, 0, 1, ex.Message);
                }

            }


        }
    }
}
