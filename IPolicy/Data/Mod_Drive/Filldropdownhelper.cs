﻿//using IPolicy.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace IPolicy.Data.Mod_Drive
{
    public class Filldropdownhelper
    {
        //public SelectList FillInsuredslist()
        //{
        //    var lstitems = new InsuredRepository().GetInsured();
        //    var insureds = new SelectList(lstitems, "CTINSRD_OTHERNAME", "CTINSRD_NO");
        //    return insureds;

        //}
        public List<SelectListItem> Fillstates()
        {
            List<SelectListItem> lstitem = new List<SelectListItem>();
            lstitem.Add(new SelectListItem { Text = "Abia", Value = "Abia" });
            lstitem.Add(new SelectListItem { Text = "Lagos", Value = "Lagos" });
            return lstitem;
        }
        public List<SelectListItem> FillInsuredtypes()
        {
            List<SelectListItem> lstitem = new List<SelectListItem>();
            lstitem.Add(new SelectListItem { Text = "Individual", Value = "B" });
            lstitem.Add(new SelectListItem { Text = "Corporate", Value = "A" });
            return lstitem;

        }

        public List<SelectListItem> Fillusertypes()
        {
            List<SelectListItem> lstitem = new List<SelectListItem>();
            lstitem.Add(new SelectListItem { Text = "Normal", Value = "O" });
            lstitem.Add(new SelectListItem { Text = "Super", Value = "A" });
            return lstitem;

        }

        public List<SelectListItem> Filluserdepartments()
        {
            List<SelectListItem> lstitem = new List<SelectListItem>();
            lstitem.Add(new SelectListItem { Text = "Department 1", Value = "T" });
            lstitem.Add(new SelectListItem { Text = "Department 2", Value = "B" });
            return lstitem;

        }

        public List<SelectListItem> FilluserGroups()
        {
            List<SelectListItem> lstitem = new List<SelectListItem>();
            lstitem.Add(new SelectListItem { Text = "Group A", Value = "1" });
            lstitem.Add(new SelectListItem { Text = "Group B", Value = "2" });
            lstitem.Add(new SelectListItem { Text = "Group C", Value = "3" });
            lstitem.Add(new SelectListItem { Text = "Group D", Value = "4" });
            lstitem.Add(new SelectListItem { Text = "Group E", Value = "5" });
            lstitem.Add(new SelectListItem { Text = "Group F", Value = "6" });
            lstitem.Add(new SelectListItem { Text = "Group G", Value = "7" });
            lstitem.Add(new SelectListItem { Text = "Group H", Value = "8" });
            lstitem.Add(new SelectListItem { Text = "Group I", Value = "9" });

            return lstitem;

        }




        //public SelectList fillStates()
        //{
        //    ////var states = new SelectList("", )
        //    ////{

        //    ////}

        //    //var states = new SelectList("")

        //}
    }
}