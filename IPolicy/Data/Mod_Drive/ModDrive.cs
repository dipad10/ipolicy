﻿using IPolicy.Data.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace IPolicy.Data.Mod_Drive
{
    public class ModDrive
    {
        #region RESPONSES
        public enum ErrCodeEnum
        {
            NO_ERROR = 0,
            INVALID_GUID = 200,
            INVALID_PASSWORD = 200,
            FORBIDDEN = 407,
            INVALID_RANGE = 408,
            ACCOUNT_LOCKED = 400,
            ACCOUNT_EXPIRED = 400,
            GENERIC_ERROR = 100
        }

        public class Response
        {
            public int ErrorCode;
            public string ErrorMessage;
            public string ExtraMessage;
            public int TotalSuccess;
            public int TotalFailure;
     
        }

        public static Response _GetResponseStruct(ErrCodeEnum ErrCode, int TotalSuccess = 0, int TotalFailure = 0, string ErrorMsg = "", string ExtraMsg = "")
        {
            Response res = new Response
                {
                ErrorCode = Convert.ToInt32(ErrCode),
                ErrorMessage = ErrorMsg,
                ExtraMessage = ExtraMsg,
                TotalSuccess = TotalSuccess,
                TotalFailure = TotalFailure

            };
   
            return res;
        }
        #endregion
        #region DATA ENCRYPTION

        public string Encrypt(string clearText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6E, 0x20, 0x4D, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }


        public string Decrypt(string cipherText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6E, 0x20, 0x4D, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }


        #endregion

        #region GET FUNCTIONS
        public List<int> Getlistofyears()
        {

            return Enumerable.Range(1995, DateTime.Now.Year - 1994).OrderByDescending(i => i).ToList();
        }

        public string Getcurrentuser()
        {
            return HttpContext.Current.Session["currentuser"].ToString();
        }
        #endregion

        #region AUTONUMBERS
        public string GetinsuredCNT()
        {
            Ipolicy_DBEntities db = new Ipolicy_DBEntities();
            var auto = db.ABSINSRDCNTs.FirstOrDefault();
            
            string format = "0000000";
            string autonumber = format + auto.AUTOINSRD_REC_NUM;
            return autonumber;
        }


        public void UpdateinsuredCNT()
        {
            Ipolicy_DBEntities db = new Ipolicy_DBEntities();
            var auto = db.ABSINSRDCNTs.FirstOrDefault();
            auto.AUTOINSRD_REC_NUM = auto.AUTOINSRD_REC_NUM + 1;
            db.SaveChanges();
        }

        public string GetpassCNT()
        {
            Ipolicy_DBEntities db = new Ipolicy_DBEntities();
            var auto = db.ABSPASSCNTs.FirstOrDefault();

            string format = "";
            string autonumber = format + auto.AUTOPASS_REC_NUM;
            return autonumber;
        }

        public void UpdatepassCNT()
        {
            Ipolicy_DBEntities db = new Ipolicy_DBEntities();
            var auto = db.ABSPASSCNTs.FirstOrDefault();
            auto.AUTOPASS_REC_NUM = auto.AUTOPASS_REC_NUM + 1;
            db.SaveChanges();
        }



        #endregion
    }
}