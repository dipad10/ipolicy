//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IPolicy.Data.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class ABSPOLYRENEWED
    {
        public string PA_REN_POL_ID { get; set; }
        public string PA_REN_POL_NUM { get; set; }
        public Nullable<decimal> PA_REN_REF_NO { get; set; }
        public string PA_REN_PROC_DATE { get; set; }
        public Nullable<decimal> PA_REN_BATCH_NUM { get; set; }
        public string PA_REN_BRA_NUM { get; set; }
        public string PA_REN_RISK_NUM { get; set; }
        public string PA_REN_SUBRISK_NUM { get; set; }
        public string PA_REN_AGCY_NUM { get; set; }
        public Nullable<System.DateTime> PA_REN_START_DATE { get; set; }
        public Nullable<System.DateTime> PA_REN_END_DATE { get; set; }
        public Nullable<System.DateTime> PA_REN_RW_START_DATE { get; set; }
        public Nullable<System.DateTime> PA_REN_RW_END_DATE { get; set; }
        public Nullable<decimal> PA_REN_SUM_INS { get; set; }
        public Nullable<decimal> PA_REN_PREM { get; set; }
        public string PA_REN_STATUS { get; set; }
        public string PA_REN_DISPATCH_YN { get; set; }
        public string PA_REN_PAYMENT_YN { get; set; }
        public string PA_REN_LAPSE { get; set; }
        public string PA_REN_DOC_NUM { get; set; }
        public string PA_REN_RCT_NUM { get; set; }
        public string PA_REN_OPERID { get; set; }
    }
}
