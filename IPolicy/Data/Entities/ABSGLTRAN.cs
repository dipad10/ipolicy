//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IPolicy.Data.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class ABSGLTRAN
    {
        public string ACT_PROC_DATE { get; set; }
        public string ACT_BRANCH_NUM { get; set; }
        public string ACT_BATCH_NUM { get; set; }
        public decimal ACT_SERIAL_NUM { get; set; }
        public decimal ACT_SUB_SERIAL { get; set; }
        public decimal ACT_REC_NO { get; set; }
        public Nullable<decimal> ACT_UNIT_NUM { get; set; }
        public string ACT_TRANS_ID { get; set; }
        public Nullable<System.DateTime> ACT_TRANS_DATE { get; set; }
        public string ACT_TRANS_NUM { get; set; }
        public string ACT_TRANS_TYPE { get; set; }
        public string ACT_RECORD_TYPE { get; set; }
        public string ACT_LEDGER_TYPE { get; set; }
        public string ACT_DRCR_CODE { get; set; }
        public string ACT_ACC_NUM { get; set; }
        public Nullable<decimal> ACT_TRANS_AMT { get; set; }
        public string ACT_CASH_CHQ { get; set; }
        public string ACT_CHQ_NUM { get; set; }
        public Nullable<System.DateTime> ACT_CHQ_DATE { get; set; }
        public string ACT_ACC_NUM_DR { get; set; }
        public string ACT_ACC_NUM_CR { get; set; }
        public string ACT_DR_CODE { get; set; }
        public string ACT_CR_CODE { get; set; }
        public string ACT_LEDGER_TYPE_DR { get; set; }
        public string ACT_LEDGER_TYPE_CR { get; set; }
        public string ACT_CURRENCY_NUM { get; set; }
        public Nullable<decimal> ACT_CURRENCY_RATE { get; set; }
        public Nullable<decimal> ACT_LOCAL_AMT { get; set; }
        public string ACT_TRANS_DESCR { get; set; }
        public string ACT_BENEFICIARY_NAME { get; set; }
        public string ACT_POLICY_NUM { get; set; }
        public string ACT_SUBRISK_NUM { get; set; }
        public string ACT_REF_DATE { get; set; }
        public string ACT_REF_NUM { get; set; }
        public string ACT_REF_TYPE { get; set; }
        public Nullable<decimal> ACT_REF_QTY { get; set; }
        public Nullable<decimal> ACT_REF_RATE { get; set; }
        public string ACT_REF_DATE2 { get; set; }
        public string ACT_REF_NUM2 { get; set; }
        public string ACT_REF_TYPE2 { get; set; }
        public Nullable<decimal> ACT_REF_AMT { get; set; }
        public string ACT_FILLER { get; set; }
        public string ACT_FLAG { get; set; }
        public Nullable<System.DateTime> ACT_KEYDTE { get; set; }
        public string ACT_OPERID { get; set; }
    }
}
