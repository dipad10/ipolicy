//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IPolicy.Data.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class AAA_CUST
    {
        public int cust_id { get; set; }
        public string cust_name { get; set; }
        public string cust_address { get; set; }
        public Nullable<decimal> cust_credit_limit { get; set; }
    }
}
