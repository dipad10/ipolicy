//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IPolicy.Data.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class ABSDBCRTAB_CRU_NEW
    {
        public string DNCN_REC_ID { get; set; }
        public string DNCN_PROCESS_DATE { get; set; }
        public string DNCN_PROC_DATE { get; set; }
        public int DNCN_BATCH_NUM { get; set; }
        public int DNCN_SERIAL_NUM { get; set; }
        public string DNCN_CODE { get; set; }
        public string DNCN_TYPE { get; set; }
        public Nullable<System.DateTime> DNCN_TRANS_DATE { get; set; }
        public string DNCN_TRANS_NUM { get; set; }
        public string DNCN_REFCODE { get; set; }
        public Nullable<System.DateTime> DNCN_REF_DATE { get; set; }
        public string DNCN_REF_NUM { get; set; }
        public Nullable<System.DateTime> DNCN_BILLING_DATE { get; set; }
        public string DNCN_BUS_SOURCE { get; set; }
        public string DNCN_BUS_TYPE { get; set; }
        public string DNCN_POL_NUM { get; set; }
        public Nullable<System.DateTime> DNCN_START_DATE { get; set; }
        public Nullable<System.DateTime> DNCN_END_DATE { get; set; }
        public string DNCN_UWR_YEAR { get; set; }
        public string DNCN_RISK_NUM { get; set; }
        public string DNCN_RISK_NUMX { get; set; }
        public string DNCN_SUBRISK_NUM { get; set; }
        public string DNCN_BRA_NUM { get; set; }
        public string DNCN_LOC_NUM { get; set; }
        public string DNCN_AGCY_NUM { get; set; }
        public int DNCN_AGCY_RATE { get; set; }
        public Nullable<double> PROP { get; set; }
        public string DNCN_INPUT_TYPE { get; set; }
        public string DNCN_VAT_YESNO { get; set; }
        public int DNCN_VAT_RATE { get; set; }
        public int DNCN_PRORATA_RDAY { get; set; }
        public int DNCN_PRORATA_NDAY { get; set; }
        public int DNCN_SUM_INS { get; set; }
        public Nullable<double> DNCN_PREMIUM { get; set; }
        public Nullable<double> DNCN_TRANS_AMT { get; set; }
        public int DNCN_FAC_BAL { get; set; }
        public string DNCN_FLAG { get; set; }
        public Nullable<System.DateTime> DNCN_KEYDTE { get; set; }
        public string DNCN_OPERID { get; set; }
        public string CTBS_LONG_DESCR { get; set; }
        public string CTBS_SHORT_DESCR { get; set; }
        public string CTBRA_NAME { get; set; }
        public string CTAGCY_NAME { get; set; }
        public string CTSUBRISK_DESCR { get; set; }
        public string PT_INS_NUM { get; set; }
        public string PT_POL_NUM { get; set; }
        public string PT_INS_NAME { get; set; }
        public string PT_INS_OTHERNAME { get; set; }
        public string CTRISK_DESCR { get; set; }
    }
}
