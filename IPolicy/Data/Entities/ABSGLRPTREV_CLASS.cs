//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IPolicy.Data.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class ABSGLRPTREV_CLASS
    {
        public decimal RPT_CLASS_NO { get; set; }
        public string RPT_CLASS_ID { get; set; }
        public string RPT_CLASS_NUM { get; set; }
        public string RPT_CLASS_DESCR { get; set; }
        public Nullable<decimal> RPT_CLASS_COL { get; set; }
        public string RPT_CLASS_FLAG { get; set; }
        public Nullable<System.DateTime> RPT_CLASS_KEYDTE { get; set; }
        public string RPT_CLASS_OPERID { get; set; }
    }
}
