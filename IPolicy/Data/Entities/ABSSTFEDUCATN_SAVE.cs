//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IPolicy.Data.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class ABSSTFEDUCATN_SAVE
    {
        public string G13_STAFF_NO { get; set; }
        public string G13_1ST_QUALIF_CODE { get; set; }
        public string G13_1ST_DISCIP_CODE { get; set; }
        public Nullable<System.DateTime> G13_1ST_QUALIF_DATE { get; set; }
        public string G13_2ND_QUALIF_CODE { get; set; }
        public string G13_2ND_DISCIP_CODE { get; set; }
        public Nullable<System.DateTime> G13_2ND_QUALIF_DATE { get; set; }
        public string G13_3RD_QUALIF_CODE { get; set; }
        public string G13_3RD_DISCIP_CODE { get; set; }
        public Nullable<System.DateTime> G13_3RD_QUALIF_DATE { get; set; }
        public string G13_4TH_QUALIF_CODE { get; set; }
        public string G13_4TH_DISCIP_CODE { get; set; }
        public Nullable<System.DateTime> G13_4TH_QUALIF_DATE { get; set; }
        public string G13_5TH_QUALIF_CODE { get; set; }
        public string G13_5TH_DISCIP_CODE { get; set; }
        public Nullable<System.DateTime> G13_5TH_QUALIF_DATE { get; set; }
        public string G13_6TH_QUALIF_CODE { get; set; }
        public string G13_6TH_DISCIP_CODE { get; set; }
        public Nullable<System.DateTime> G13_6TH_QUALIF_DATE { get; set; }
        public string G13_TAG { get; set; }
        public string G13_FLAG { get; set; }
        public Nullable<System.DateTime> G13_KEYDTE { get; set; }
        public string G13_OPERID { get; set; }
    }
}
