//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IPolicy.Data.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class ABSFILESTATU
    {
        public string FLOCK_ID { get; set; }
        public string FLOCK_DATE { get; set; }
        public decimal FLOCK_NO { get; set; }
        public string FLOCK_STATUS { get; set; }
        public string FLOCK_REMARKS { get; set; }
        public string FLOCK_FLAG { get; set; }
        public Nullable<System.DateTime> FLOCK_DTE { get; set; }
        public string FLOCK_OPERID { get; set; }
    }
}
