//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IPolicy.Data.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class ABSRECEIPT_COUNTER
    {
        public string AUTO_RCT_REC_ID { get; set; }
        public string AUTO_RCT_BRA_NUM { get; set; }
        public Nullable<decimal> AUTO_RCT_NUM { get; set; }
    }
}
