//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IPolicy.Data.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class ABSSTFDISPUTE
    {
        public System.DateTime DISPUTE_DATE { get; set; }
        public string DISPUTE_STAFF_NUM { get; set; }
        public decimal DISPUTE_REC_NO { get; set; }
        public string DISPUTE_ISSUES { get; set; }
        public string DISPUTE_ACTION_TAKEN { get; set; }
        public Nullable<System.DateTime> DISPUTE_KEYDTE { get; set; }
        public string DISPUTE_FLAG { get; set; }
        public string DISPUTE_OPERID { get; set; }
    }
}
