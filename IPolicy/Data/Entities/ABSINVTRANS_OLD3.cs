//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IPolicy.Data.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class ABSINVTRANS_OLD3
    {
        public string INV_TRANS_REC_ID { get; set; }
        public string INV_TRANS_MST_NUM { get; set; }
        public string INV_TRANS_CERT_NUM { get; set; }
        public string INV_TRANS_TYPE { get; set; }
        public System.DateTime INV_TRANS_DATE { get; set; }
        public decimal INV_TRANS_REC_NO { get; set; }
        public string INV_TRANS_NUM { get; set; }
        public string INV_TRANS_RETIRE_YN { get; set; }
        public Nullable<System.DateTime> INV_TRANS_RETIRE_DATE { get; set; }
        public Nullable<decimal> INV_TRANS_INTEREST_RATE { get; set; }
        public Nullable<decimal> INV_TRANS_TENURE { get; set; }
        public Nullable<decimal> INV_TRANS_AMT { get; set; }
        public Nullable<decimal> INV_TRANS_INCOME_AMT_DAY { get; set; }
        public Nullable<decimal> INV_TRANS_INCOME_AMT_TOT { get; set; }
        public Nullable<System.DateTime> INV_TRANS_MATURITY_DATE { get; set; }
        public Nullable<decimal> INV_TRANS_MATURITY_AMT { get; set; }
        public string INV_TRANS_LOC_NUM { get; set; }
        public string INV_TRANS_CAT_NUM { get; set; }
        public Nullable<System.DateTime> INV_TRANS_REF_DATE { get; set; }
        public string INV_TRANS_REF_NUM { get; set; }
        public string INV_TRANS_STATUS { get; set; }
        public string INV_TRANS_GL_ACC_DR { get; set; }
        public string INV_TRANS_GL_ACC_CR { get; set; }
        public string INV_TRANS_FLAG { get; set; }
        public Nullable<System.DateTime> INV_TRANS_KEYDTE { get; set; }
        public string INV_TRANS_OPERID { get; set; }
    }
}
