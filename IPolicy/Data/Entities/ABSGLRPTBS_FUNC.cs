//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IPolicy.Data.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class ABSGLRPTBS_FUNC
    {
        public decimal RPT_FUNC_NO { get; set; }
        public string RPT_FUNC_ID { get; set; }
        public string RPT_FUNC_NUM { get; set; }
        public string RPT_FUNC_LN { get; set; }
        public string RPT_FUNC_DESCR { get; set; }
        public string RPT_FUNC_GRP_NUM { get; set; }
        public Nullable<decimal> RPT_FUNC_COUNT { get; set; }
        public string RPT_FUNC_LINE_NUM { get; set; }
        public string RPT_FUNC_FLAG { get; set; }
        public Nullable<System.DateTime> RPT_FUNC_KEYDTE { get; set; }
        public string RPT_FUNC_OPERID { get; set; }
    }
}
