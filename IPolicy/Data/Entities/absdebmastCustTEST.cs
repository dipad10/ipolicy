//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IPolicy.Data.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class absdebmastCustTEST
    {
        public string recid { get; set; }
        public string procdate { get; set; }
        public string brchnum { get; set; }
        public string acctnum { get; set; }
        public Nullable<int> recnum { get; set; }
        public string transdate { get; set; }
        public string transnum { get; set; }
        public string trantype { get; set; }
        public string refdate { get; set; }
        public string refnum { get; set; }
        public string reftype { get; set; }
        public decimal unitnum { get; set; }
        public string insnum { get; set; }
        public string policynum { get; set; }
        public string risknum { get; set; }
        public string subrisk { get; set; }
        public string bustype { get; set; }
        public string debdescr { get; set; }
        public Nullable<int> ClaimNum { get; set; }
        public string bussource { get; set; }
        public Nullable<double> gross { get; set; }
        public decimal commrate { get; set; }
        public Nullable<double> commamt { get; set; }
        public decimal vatrate { get; set; }
        public decimal vatamt { get; set; }
        public string origdrcr { get; set; }
        public Nullable<double> netamt { get; set; }
        public Nullable<int> dropflag { get; set; }
        public string compflag { get; set; }
        public string debflag { get; set; }
        public string keydate { get; set; }
        public string operid { get; set; }
    }
}
