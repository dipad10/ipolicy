﻿using System.Web;
using System.Web.Optimization;

namespace IPolicy
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/vendors/js").Include(
                //"~/vendors/bower_components/jquery/dist/jquery.min.js",
                "~/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js",
                  "~/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js",
                  "~/dist/js/dataTables-data.js",
                    "~/dist/js/jquery.slimscroll.js",
                     "~/vendors/bower_components/waypoints/lib/jquery.waypoints.min.js",
                     "~/vendors/bower_components/jquery.counterup/jquery.counterup.min.js",
                     //"~/dist/js/dropdown-bootstrap-extended.js",

                     "~/vendors/jquery.sparkline/dist/jquery.sparkline.min.js",
                     "~/vendors/bower_components/owl.carousel/dist/owl.carousel.min.js",
                     "~/vendors/bower_components/switchery/dist/switchery.min.js",
                     "~/vendors/vectormap/jquery-jvectormap-2.0.2.min.js",
                     "~/vendors/vectormap/jquery-jvectormap-us-aea-en.js",
                     "~/vendors/vectormap/jquery-jvectormap-world-mill-en.js",
                     "~/dist/js/vectormap-data.js",
                     "~/vendors/bower_components/peity/jquery.peity.min.js",
                     "~/dist/js/peity-data.js",
                     "~/vendors/bower_components/chartist/dist/chartist.min.js",
                     "~/vendors/bower_components/raphael/raphael.min.js",
                     "~/vendors/bower_components/moment/min/moment-with-locales.min.js",
                     "~/vendors/bower_components/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js",
                     "~/vendors/bower_components/switchery/dist/switchery.min.js",
                     "~/vendors/bower_components/select2/dist/js/select2.full.min.js",
                     "~/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js",
                     "~/vendors/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js",
                     "~/vendors/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js",
                     "~/vendors/bower_components/multiselect/js/jquery.multi-select.js",
                     "~/vendors/bower_components/bootstrap-switch/dist/js/bootstrap-switch.min.js",
                     "~/dist/js/form-advance-data.js",
                     "~/vendors/bower_components/morris.js/morris.min.js",
                     "~/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js",
                     "~/dist/js/toastDemo.js",

                      "~/dist/js/bootstrap-datepicker.js",

                      "~/vendors/chart.js/Chart.min.js",
                       "~/dist/js/init.js",
                        "~/dist/js/dashboard-data.js"));

            bundles.Add(new StyleBundle("~/vendors/css").Include(
                     "~/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css",
                     "~/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.css",
                     "~/vendors/bower_components/morris.js/morris.css",
                     "~/vendors/bower_components/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css",
                     "~/vendors/bower_components/select2/dist/css/select2.min.css",
                     "~/vendors/bower_components/switchery/dist/switchery.min.css",
                     "~/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.min.css",
                     "~/vendors/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css",
                     "~/vendors/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css",
                     "~/vendors/bower_components/multiselect/css/multi-select.css",
                     "~/vendors/bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css",
                     "~/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css",
                     "~/vendors/vectormap/jquery-jvectormap-2.0.2.css",
                     "~/vendors/bower_components/chartist/dist/chartist.min.css"));

            bundles.Add(new StyleBundle("~/dist/css").Include(
                "~/dist/css/animate.css",
                   "~/dist/css/bootstrap-datepicker.css",

                      "~/dist/css/style.css"));
        }
    }
}
