﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IPolicy.Extensions
{
    public static class MakeActive
    {
        public static string MakeActiveaction(this UrlHelper urlHelper, string action)
        {
            string result = "active-page";

            string actionName = urlHelper.RequestContext.RouteData.Values["action"].ToString();

            if (!actionName.Equals(action, StringComparison.OrdinalIgnoreCase))
            {
                result = null;
            }

            return result;
        }

        public static string MakeActiveController(this UrlHelper urlHelper, string controller)
        {
            string result = "active";

            string controllerName = urlHelper.RequestContext.RouteData.Values["controller"].ToString();

            if (!controllerName.Equals(controller, StringComparison.OrdinalIgnoreCase))
            {
                result = null;
            }

            return result;
        }


    }
}