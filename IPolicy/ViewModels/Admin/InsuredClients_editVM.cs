﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IPolicy.ViewModels.Admin
{
    public class InsuredClients_editVM
    {
        public int CtinsrdNo { get; set; }
        public string CTINSRD_NUM { get; set; }

        public string CtinsrdLongDescr { get; set; }
        [Required]
        public string CtinsrdOthername { get; set; }
        public string CtinsrdAddr1 { get; set; }
        public string CtinsrdAddr2 { get; set; }

        public string CtinsrdAddr3 { get; set; }

        [Required]
        public string CtinsrdType { get; set; }
        [Required]
        public string CtinsrdOccup { get; set; }
        [Required]
        public string CtinsrdTelNum1 { get; set; }
        public string CtinsrdTelNum2 { get; set; }
        public string CtinsrdGsmNum { get; set; }
        [Required]
        public string CtinsrdEmailNum { get; set; }
        public string CtinsrdShortDescr { get; set; }
        [Required]
        public DateTime? CtinsrdDob { get; set; }
        [Required]
        public string CtinsrdGender { get; set; }
        [Required]
        public string CtinsrdOrigin { get; set; }
        [Required]
        public string CtinsrdLga { get; set; }
        public string CtinsrdBizorigin { get; set; }
        public string CtinsrdNation { get; set; }
        [Required]
        public string CtinsrdBvNum { get; set; }
      
    }
}
