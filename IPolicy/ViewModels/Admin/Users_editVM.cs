﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IPolicy.ViewModels.Admin
{
    public class Users_editVM
    {
        public int PWD_REC_NO { get; set; }

        public string PWD_ID { get; set; }
        [Required]
        public string PWD_USER_NAME { get; set; }

        public string PWD_TEL_NUM { get; set; }
        public string PWD_FAX_NUM { get; set; }
        [Required]
        public string PWD_EMAIL_NUM { get; set; }

        public string PWD_MOBILE_NUM { get; set; }
        [Required]
        public string PWD_CODE { get; set; }
        [Required]
        [Compare("PWD_CODE")]
        public string PWD_CODE_RE { get; set; }
        [Required]
        public string PWD_USER_TYPE { get; set; }
        [Required]
        public string PWD_USER_DEPT { get; set; }
        [Required]
        public decimal? PWD_USER_GRP { get; set; }
        [Required]
        public string PWD_ENABLE_SW { get; set; }
        public string PWD_SETUP_SW { get; set; }
        public string PWD_SECURITY_SW { get; set; }
        public string PWD_GENERAL_SW { get; set; }
        public string PWD_MARKETING_SW { get; set; }
        public string PWD_POLICY_SW { get; set; }
        public string PWD_UNDERWRITING_SW { get; set; }
        public string PWD_CLAIMS_SW { get; set; }
        public string PWD_REINSURANCE_SW { get; set; }
        public string PWD_CREDITCONTROL_SW { get; set; }
        public string PWD_ACCOUNTS_SW { get; set; }
        public string PWD_FIXEDASSET_SW { get; set; }
        public string PWD_PAYROLL_SW { get; set; }
        public string PWD_PERSONNEL_SW { get; set; }
        public string PWD_ACCOUNTS_SW_GL { get; set; }
        public string PWD_INVESTMENT_SW { get; set; }
        public string PWD_PURCHASES_SW { get; set; }
        public string PWD_INSERT_SW { get; set; }
        public string PWD_UPDATE_SW { get; set; }
        public string PWD_DELETE_SW { get; set; }
        public string PWD_PRINT_SW { get; set; }
        public string PWD_VIEW_SW { get; set; }
        public string PWD_PROCESS_SW { get; set; }
        public string PWD_MGT_CAT_SW { get; set; }
        public string PWD_MGT_CAT_NUM { get; set; }
        public DateTime? PWD_EXPIRY_DATE { get; set; }
        public string PWD_UND_SPC { get; set; }

    }
}