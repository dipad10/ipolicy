﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PolicyDrive.ViewModels.Home
{
    public class ForgotpwdVM
    { 
         [Required]
        public string Emailaddress { get; set; }
    }
}