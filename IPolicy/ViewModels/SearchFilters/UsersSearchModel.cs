﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IPolicy.ViewModels.SearchFilters
{
    public class UsersSearchModel
    {

        public DateTime? From { get; set; }

        public DateTime? To { get; set; }

        public string Username { get; set; }

    }
}